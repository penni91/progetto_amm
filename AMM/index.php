<!DOCTYPE html>
<html>
    <head>
        <title>Informazioni sul Progetto</title> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="author" content="Sandra Depau">
       
    </head>
    <body>
        <h2>Requisiti Soddisfatti:</h2>
        <ul>
        <li><h3>Utilizzo di HTML e CSS</h3></li>
        <li><h3>Utilizzo di PHP e MySQL</h3></li>
        <li><h3>Utilizzo del pattern MVC</h3></li>
        <li><h3>Due ruoli</h3></li>
        </ul>
        <h2>L'aplicazione gestisce due tipi di utenti:</h2>
        <ul>
        <li><h3>acquirente</h3></li>
        <li><h3>amministratore</h3></li>
        </ul>

        <p>L' acquirente può registrarsi, consultare i prodotti inseriti dall'amministratore del sito<br>
            e aggiungerli al carrello per poi procedere nell'acquisto.<br> 
            Inoltre può verificare la cronologia dei prodotti acquistati, i propri dati personali e <br>
            ricaricare il proprio saldo disponibile.
        </p>
       
        
        <p>L' amministratore può accedere a tutte le funzionalità dell'acquirente e può inserire nuovi <br>
            prodotti da mettere in viendita.
        </p>
        
        <h3>Credenziali:</h3>
        <ul>
            <li>
                <h3>Admin</h3>
                <ul>
                    <li>Username: admin</li>
                    <li>Password: admin</li>
                </ul>
            </li>
            
            <li>
                <h3>Acquirente</h3>
                <ul>
                    <li>Username: client</li>
                    <li>Password: client</li>
                </ul>
            </li>

        </ul>
        
<a href="mvc/index.php?page=login">Vedi l'applicazione</a>

    </body>
</html>
