<?php
include_once ("control/ControlLogin.php");
include_once ("control/ControlProdotti.php");
include_once ("control/ControlCarrello.php");
include_once ("control/ControlProfilo.php");

if(isset($_REQUEST['page'])){
    switch($_REQUEST['page']){
        case 'login':
            
            $controller = new ControlLogin();
            $controller->start();
            
            break;
        case 'registra':
            
            $controller = new ControlRegistra();
            $controller->start();
            
            break;
       case 'prodotti':
            $controller = new ControlProdotti();
            $controller->invoke();
                      
        break;
    case 'carrello':
            $controller = new ControlCarrello();
            $controller->invoke();
                      
        break;
    
    case 'home':
            if(isset($_REQUEST['user'])){
                $utente=$_REQUEST['user'];
            include 'view/VistaHome.php';
            }
                      
        break;
    case 'mycosmetici':
            $controller = new ControlProfilo();
            $controller->invoke();
                      
        break;
    case 'nuovoProdotto':
            $controller = new ControlProdotti();
            $controller->invoke();
                      
        break;
    }
}


?>
