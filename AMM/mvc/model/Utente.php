<?php
include_once("model/mySql.php");
class Utente {
public $username;
public $nome;
public $cognome;
private $password;
public $ruolo;
public $saldo;
public $indirizzo;



public function __construct(){  }

public function setUsername($username){
    $this->username = $username;
}
public function setSaldo($saldo){
    $this->saldo = $saldo;
}
public function setNome($nome){
    $this->nome = $nome;
}
public function setCognome($cognome){
    $this->cognome = $cognome;
}
public function setRuolo($ruolo){
    $this->ruolo = $ruolo;
}
public function setPassword($password){
    $this->password= $password;
}
public function getUsername($username){
    return $this->username;
}
public function getNome(){
    return $this->nome;
}
public function getCognome(){
    return $this->cognome;
}
public function getRuolo(){
    return $this->ruolo;
}
public function getPassword(){
    return $this->password;
}

public function getSaldo(){
    return $this->saldo;
}
public function setIndirizzo($indirizzo){
    $this->indirizzo=$indirizzo;
}


//retituisci
public function getIndirizzo(){
    return $this->indirizzo;
}


 function logout(){
     $_SESSION=array();
     if(session_id()!=""||isset($_COOKIE[session_name()])){
         setcookie(session_name(),'',time()-2592000,'/');
     }
     session_destroy();
}



}



?>
