<!DOCTYPE html>
<html>
    <head>
        <title>Compra online i tuoi cosmetici</title>
        
        <meta name="author" content="Sandra">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/stile.css" type="text/css">
        <script type="text/javascript" src="../lib/jquery.js"></script>
    </head>
    <body>
        
        <div id="page"> 
            <div id="header"> 
                <div id="titolo">
						<br>
                                                 <a href="./index.php?page=login&logout=si"><img src="../img/esci.png" alt="esci" width="60" height="60" align="right"></a>
                   
						<h1>Cosmetici Online</h1>
						  <h2>Guarda il nostro negozio...</h2>
                                       
                </div>
                 <div id="navigazione">
                    <ul>                     
			<?php
                        echo '<li> <a href="./index.php?page=home&user='.$utente.'"> Home</a> </li>';
                        echo'<li class="attuale"> <a href="./index.php?page=prodotti&user='.$utente.'">Prodotti</a></li>';
                        echo'<li> <a href="./index.php?page=carrello&user='.$utente.'">Carrello</a></li>';
                        echo'<li> <a href="./index.php?page=mycosmetici&subpage=vedi&user='.$utente.'">MyCosmetici</a></li>';
                        ?>
                    </ul>
                </div>
            </div>
            <div id="container">
                <div id="sidebar1">
                    <ul>
                        <?php
                        echo '<li> <a href="./index.php?page=home&user='.$utente.'"> Home</a> </li>';
                        echo'<li class="attuale"> <a href="./index.php?page=prodotti&user='.$utente.'">Prodotti</a></li>';
                        echo'<li> <a href="./index.php?page=carrello&user='.$utente.'">Carrello</a></li>';
                        echo'<li> <a href="./index.php?page=mycosmetici&subpage=vedi&user='.$utente.'">MyCosmetici</a></li>';
                        ?>
                    </ul>
                </div>
          
                <div id="sidebar2">
				<img src="../img/domanda.png" alt="domanda" width="40" height="60" align="left" />
                    
                    <ul>
                        <li><u>Sei nella sezione <b>Prodotti</b></u></li>
                        <li>Cliccando su <b>Carrello</b> vedrai l'ammontare del tuo ordine e i prodotti inseriti.</li>
                        <li>Nella sezione <b>MyCosmetici</b> potrai verificare la cronologia dei tuoi ordini.</li>
                    </ul>
                </div>
                <div id="content">
                     <table id="minitable">
                       
                            <th>Codice</th>
                            <th>Tipo</th>
                            <th>Descrizione</th>
                            <th>Prezzo</th>
                            <th>Aggiungi</th>
                    <?php 
                            while($cosmetico= $uno->fetch_object()){
                             
                             
                             echo '<tr class="color"><td>'.$cosmetico->codice.'</td><td>'.$cosmetico->tipo.'</td><td>'.$cosmetico->descrizione.'</td><td>€ '.$cosmetico->prezzo.'</td><td> <a href="index.php?page=carrello&user='.$utente.'&add='.$cosmetico->codice.'">+</a></td></tr>';
                            }
                            
                     ?>
                  </table>
                    <br>
                    
                    
                   
                </div>
                <div id="push"> </div>
            </div>
            <div id="footer">
                Contatti: <br>
                N.Tel 123456    email sandra@sandra.com   
               </div>
        </div>
    </body>
</html>

                   
  


