<!DOCTYPE html>
<html>
    <head>
        <title>Compra online i tuoi cosmetici</title>
        
        <meta name="author" content="Sandra">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/stile.css" type="text/css">
        <script type="text/javascript" src="../lib/jquery.js"></script>
    </head>
    <body>
        
        <div id="page"> 
            <div id="header"> 
                <div id="titolo">
                    <br>
                    
                     <a href="./index.php?page=login&logout=si"><img src="../img/esci.png" alt="esci" width="60" height="60" align="right"></a>
                   
						<h1>Cosmetici Online</h1>
						  <h2>Guarda il nostro negozio...</h2>
                                       
                </div>
                 <div id="navigazione">
                    <ul>                     
			<?php
                        echo '<li> <a href="./index.php?page=home&user='.$utente.'"> Home</a> </li>';
                        echo'<li> <a href="./index.php?page=prodotti&user='.$utente.'">Prodotti</a></li>';
                        echo'<li class="attuale"> <a href="./index.php?page=carrello&user='.$utente.'">Carrello</a></li>';
                        echo'<li> <a href="./index.php?page=mycosmetici&subpage=vedi&user='.$utente.'">MyCosmetici</a></li>';
                        ?>
                    </ul>
                </div>
            </div>
            <div id="container">
                <div id="sidebar1">
                    <ul>
                        <?php
                        echo '<li> <a href="./index.php?page=home&user='.$utente.'"> Home</a> </li>';
                        echo'<li> <a href="./index.php?page=prodotti&user='.$utente.'">Prodotti</a></li>';
                        echo'<li class="attuale"> <a href="./index.php?page=carrello&user='.$utente.'">Carrello</a></li>';
                        echo'<li> <a href="./index.php?page=mycosmetici&subpage=vedi&user='.$utente.'">MyCosmetici</a></li>';
                        ?>
						
                    </ul>
                </div>
          
                <div id="sidebar2">
				<img src="../img/domanda.png" alt="domanda" width="40" height="60">
                    
                    <ul>
                        <li><u>Stai visualizzando l'ammontare del tuo ordine e i prodotti inseriti.</u></li>
						<li> Vai nella sezione <b>Prodotti</b> per consulatre la nostra vasta varietà di cosmetici.</li>
                        <li>Nella sezione <b>MyCosmetici</b> potrai verificare la cronologia dei tuoi ordini.</li>
                    </ul>
                </div>
                <div id="content">
                    <table id="tabella_Disponibili">
                       
                            <th>Codice</th>
                            <th>Tipo</th>
                            <th>Descrizione</th>
                            <th>Prezzo</th>
                            <th>Togli</th>
                            <?php 
                                $i=0;
                                $tot=0;
                                while($row = $cosmetici->fetch_array()){ //per ogni prodotto nel database
                                    
                                    
                                    $query = "SELECT * FROM prodotti WHERE prodotti.codice=".$row['codice']; //carico il prod da quelli in vendita

                                     $result = (mySql::executeQuery($query,$codiceerr));
                                     if (isset($result))
                                     while($row2 = $result->fetch_array()){
                                     
                                     $cosmetico= new Cosmetico();
                                     $cosmetico->setCodice($row2['codice']);
                                     $cosmetico->setTipo ($row2['tipo']);
                                     $cosmetico->setDescrizione($row2['descrizione']);
                                     $cosmetico->setPrezzo($row2['prezzo']);
                                     
                                     if(($i%2)==0)
                                           {
                                            echo '<tr class="color"><td><a href="index.php?page=prodotti&user='.$utente.'&cosmetico='.$cosmetico->codice.'">'.$cosmetico->codice.'</a></td><td>'.$cosmetico->tipo.'</td><td>'.$cosmetico->descrizione.'</td><td>'.$cosmetico->prezzo.'</td><td> <a href="index.php?page=carrello&user='.$utente.'&meno='.$cosmetico->codice.'">-</a></td></tr>';
                                      }else{
                                            echo '<tr><td><a href="index.php?page=prodotti&user='.$utente.'&cosmetico='.$cosmetico->codice.'">'.$cosmetico->codice.'</a></td><td>'.$cosmetico->tipo.'</td><td>'.$cosmetico->descrizione.'</td><td>'.$cosmetico->prezzo.'</td><td> <a href="index.php?page=carrello&user='.$utente.'&meno='.$cosmetico->codice.'">-</a></td></tr>';
                                      } 
                                                $i++;//fine visualizzazione
                                    $tot=$tot+$cosmetico->prezzo;
                                     }//2 while 
                                     
                                }//fine primo while
                                

                            ?>
                    
                     
                    </table>
                    
                    <table>
                        <th>TOTALE ORDINE:<?php  echo ' €  '.$tot.' ' ?></th><th><?php echo'<a href="index.php?page=mycosmetici&subpage=acquisto&user='.$utente.'&tot='.$tot.'">ACQUISTA</a>';  ?>  </th> 
                    </table>
                </div>
                <div id="push"> </div>
            </div>
            <div id="footer">
                Contatti: <br>
                N.Tel 123456    email sandra@sandra.com   
                 </div>
        </div>
    </body>
</html>

