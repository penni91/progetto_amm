<!DOCTYPE html>
<html>
    <head>
        <title>Compra online i tuoi cosmetici</title>
        
        <meta name="author" content="Sandra">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/stile.css" type="text/css">
        <script type="text/javascript" src="../lib/jquery.js"></script>
    </head>
    <body>
        
        <div id="page"> 
            <div id="header"> 
                <div id="titolo">
                    <br>
                     <a href="./index.php?page=login&logout=si"><img src="../img/esci.png" alt="esci" width="60" height="60" align="right"></a>
                   
						<h1>Cosmetici Online</h1>
						  <h2>Guarda il nostro negozio...</h2>
                                       
                </div>
                 <div id="navigazione">
                    <ul>                     
			<?php
                        echo '<li> <a href="./index.php?page=home&user='.$utente.'"> Home</a> </li>';
                        echo'<li> <a href="./index.php?page=prodotti&user='.$utente.'">Prodotti</a></li>';
                        echo'<li> <a href="./index.php?page=carrello&user='.$utente.'">Carrello</a></li>';
                        echo'<li class="attuale"> <a href="./index.php?page=mycosmetici&subpage=vedi&user='.$utente.'">MyCosmetici</a></li>';
                        ?>
                    </ul>
                </div>
            </div>
            <div id="container">
                <div id="sidebar1">
                    <ul>
                        <?php
                        echo '<li> <a href="./index.php?page=home&user='.$utente.'"> Home</a> </li>';
                        echo'<li> <a href="./index.php?page=prodotti&user='.$utente.'">Prodotti</a></li>';
                        echo'<li> <a href="./index.php?page=carrello&user='.$utente.'">Carrello</a></li>';
                        echo'<li class="attuale"> <a href="./index.php?page=mycosmetici&subpage=vedi&user='.$utente.'">MyCosmetici</a></li>';
                        ?>
                    </ul>
                </div>
          
                <div id="sidebar2">
				<img src="../img/domanda.png" alt="domanda" width="40" height="60" align="left" />
                    
                    <ul>
                        <li><u>Stai visualizzando la cronologia dei tuoi prodotti acquistati.</u></li>
						  <li> Vai nella sezione <b>Prodotti</b> per consulatre la nostra vasta varietà di cosmetici.</li>
                        <li>Cliccando su <b>Carrello</b> vedrai l'ammontare del tuo ordine e i prodotti inseriti.</li>
                    </ul>
                </div>
                <div id="content">
                      <table id="tabella_Disponibili">
                        <tr>
                            <th>Codice</th>
                            <th>Tipo</th>
                            <th>Descrizione</th>
                            <th>Prezzo</th>
							
                        </tr>
                        <?php 
                                if(isset($cosmetici)){
                                $i=0;
                                
                                while($row = $cosmetici->fetch_array()){ //per ogni prodotto nel database
                                    
                                    
                                    $query = "SELECT * FROM prodotti WHERE prodotti.codice=".$row['codice']; //carico il prod da quelli in vendita

                                     $result = (mySql::executeQuery($query,$codiceerr));
                                     if (isset($result))
                                     while($row2 = $result->fetch_array()){
                                     
                                     $cosmetico= new Cosmetico();
                                     $cosmetico->setCodice($row2['codice']);
                                     $cosmetico->setTipo ($row2['tipo']);
                                     $cosmetico->setDescrizione($row2['descrizione']);
                                     $cosmetico->setPrezzo($row2['prezzo']);
                                     
                                     if(($i%2)==0)
                                           {
                                            echo '<tr class="color"><td>'.$cosmetico->codice.'</a></td><td>'.$cosmetico->tipo.'</td><td>'.$cosmetico->descrizione.'</td><td>'.$cosmetico->prezzo.'</td></tr>';
                                      }else{
                                            echo '<tr><td>'.$cosmetico->codice.'</td><td>'.$cosmetico->tipo.'</td><td>'.$cosmetico->descrizione.'</td><td>'.$cosmetico->prezzo.'</td></tr>';
                                      } 
                                                $i++;//fine visualizzazione
                                    
                                     }//2 while 
                                     
                                }//fine primo while
                                }else{
                                    echo 'NON HAI ANCORA EFFETTUATO NESSUN ACQUISTO';
                                }
                             
                        ?>
                     
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <hr>
                    <table>
                        <th>Nome</th>
                        <th>Cognome</th>
                    <?php
                                    $query = "SELECT * FROM utenti WHERE utenti.user='$utente'"; 

                                     $result = (mySql::executeQuery($query,$codiceerr));
                                     if (isset($result))
                                     while($row3 = $result->fetch_array()){
                                      
                                     
                                            echo '<tr class="color"><td>'.$row3['nome'].'</td><td>'.$row3['cognome'].'</td></tr>';
                                   
                                          
                                      
                                     }
                                     
                        
                    ?>
                    </table>
                    <table>
                        <th>Indirizzo</th>
                        <th>Saldo</th>
                    <?php
                       $query = "SELECT * FROM utenti WHERE utenti.user='$utente'"; 

                                     $result = (mySql::executeQuery($query,$codiceerr));
                                     if (isset($result))
                                     while($row4 = $result->fetch_array()){
                    
                                     
                                    
                                            echo '<tr class="color"><td>'.$row4['indirizzo'].'</td><td>'.$row4['saldo'].'</td></tr>';
                                      
                                     }   
                    ?>
                    </table>
                    <hr>
                    RICARICA SALDO:<br>
                       <div id="ricarica">
                        <?php
                        echo'<form action="./index.php?page=mycosmetici&subpage=ricarica&user='.$utente.'" method="POST">';
                        echo'<label for="codice_carta">Codice Carta:</label>';
                        echo'   <input type="text" id="codice_carta" name="codice_carta">';
                        echo'   <label for="importo">Importo Ricarica:</label>';
                        echo'   <input type="text" id="importo" name="importo">';




                        echo' <input type="hidden" name="ricarica" value="true">';
                        echo'<input type="submit" value="Ricarica">';

                        echo'</form>';
                                ?>
                    </div>
                </div>
                <div id="push"> </div>
            </div>
            <div id="footer">
                Contatti: <br>
                N.Tel 123456    email sandra@sandra.com   
            </div>
        </div>
    </body>
</html>
