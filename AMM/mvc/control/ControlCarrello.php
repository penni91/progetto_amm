<?php
include_once("model/Carrello.php"); 
include_once("model/mySql.php");
include_once("model/Utente.php");

class ControlCarrello {
 public $model; 

 public function __construct()  
 {  
    $this->model = new Carrello(); 
 }
 

 public function invoke()
 {  
     
      
     if(isset($_REQUEST['user'])){
          $utente=$_REQUEST['user'];
          $codiceerr = null;
     if(!isset($_REQUEST['add']))
    {
        //  mostro tutti i prod disponibili
        $cosmetici = $this->model->carica($utente, $codiceerr);
        if(isset($cosmetici))
        include 'view/VisualizzaCarrello.php';
    }
    else 
   {
      $carrello = $this->model->addCarrello($_REQUEST['add'], $codiceerr, $utente);
      $cosmetici = $this->model->carica($utente,$codiceerr);
      if(isset($cosmetici))
      include 'view/VisualizzaCarrello.php';
   }
   if(isset($_REQUEST['meno'])){
      $carrello = $this->model->togliCarrello($_REQUEST['meno'], $codiceerr, $utente);
      $cosmetici = $this->model->carica($utente,$codiceerr);
      if(isset($cosmetici))
      include 'view/VisualizzaCarrello.php';
   }
     }else         echo 'erroreeee';
 }
 
 
 }//fine
 


?>